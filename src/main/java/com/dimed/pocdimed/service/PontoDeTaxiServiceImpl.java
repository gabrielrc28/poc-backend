package com.dimed.pocdimed.service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dimed.pocdimed.model.PontoDeTaxi;
import com.dimed.pocdimed.repository.PontoDeTaxiRepository;

@Service
public class PontoDeTaxiServiceImpl implements PontoDeTaxiService {

	@Autowired
	PontoDeTaxiRepository pontoDeTaxiRepository;
	
	@Override
	public List<PontoDeTaxi> getAllPontosTaxi() throws IOException
	{
		return pontoDeTaxiRepository.findAll();
	}
	
	@Override
	public PontoDeTaxi getPontoDeTaxiByName (String nome) throws IOException{
		
		List<PontoDeTaxi> pontosDeTaxi = pontoDeTaxiRepository.findAll();
		PontoDeTaxi pontoDeTaxi = new PontoDeTaxi();
		
		for(PontoDeTaxi aux : pontosDeTaxi)
		{
			if(aux.getNome().equals(nome))
			{
				pontoDeTaxi =  aux;
			}
		}
		return pontoDeTaxi;
	}
	
	@Override
	public void insertPontoDeTaxi (PontoDeTaxi pontoDeTaxi) throws IOException {
		FileWriter fileWriter = new FileWriter(".\\pontosDeTaxi.tx", true);
		
		PrintWriter printWriter = new PrintWriter(fileWriter);
		
		printWriter.print((pontoDeTaxi.getNome() + "#").toUpperCase());
		printWriter.print(pontoDeTaxi.getLat() + "#");
		printWriter.print(pontoDeTaxi.getLng() + "#");
		printWriter.println(pontoDeTaxi.getData());
		
		printWriter.flush();
		fileWriter.close();
		
		pontoDeTaxiRepository.save(pontoDeTaxi);
	}
}
