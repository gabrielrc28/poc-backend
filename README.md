# poc-backend

Escolhi usar o Spring Boot, JPA, H2 e o Gradle, pois, estou mais familiarizado com eles.


*Requests*
http://localhost:8080/taxi/pontos Retorna todos os pontos de taxi cadastrados.

http://localhost:8080/taxi/cadastro Faz o cadastro de um novo ponto (recebe uma Entity PontoDeTaxi).

http://localhost:8080/linhas Retorna todas as linhas de onibus cadastradas (consulta pela API).

http://localhost:8080/linhas/nome/?name= {nome} Retorna linhas que contenham o nome que foi passado por parâmetro (consulta pela API).

http://localhost:8080/linhas/bd/inserir_linha Salva uma linha no banco (recebe uma Entity LinhaOnibus).

http://localhost:8080/linhas/bd/linhas Retorna todas as linha cadastradas no banco.

http://localhost:8080/linhas/bd/deletar_linha/id?id=5000 Deleta uma linha por ID no banco.

http://localhost:8080/linhas/bd/id?id=5566 Retorna uma linha salva no banco.

http://localhost:8080/linhas/filtra?lat=-30.035894618888562&lng=-51.2040869808423&dist=2 Passa latitude e longitude e distancia do raio para filtrar as rotas que operam dentro desse raio.

http://localhost:8080/rota/id?id=5566 Retorna uma rota com as suas coordenadas(consulta pela API).

http://localhost:8080/rota/db/id?id=5566 Retorna uma rota com as suas coordenadas(Consulta o banco).

http://localhost:8080/rota/db/inserir_rota {RotaOnibus} Salva uma rota com suas coordenadas no banco.

http://localhost:8080/rota/db/deletar_rota/id?id=5566 Deleta uma rota do banco.

build gradle

docker build -f Dockerfile -t poc .

docker-compose up
