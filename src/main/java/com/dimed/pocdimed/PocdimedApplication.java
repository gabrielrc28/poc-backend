package com.dimed.pocdimed;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.dimed.pocdimed.model.PontoDeTaxi;
import com.dimed.pocdimed.repository.PontoDeTaxiRepository;

@SpringBootApplication
public class PocdimedApplication implements CommandLineRunner {

	@Autowired
	PontoDeTaxiRepository pontoDeTaxiRepository;
	
	@Bean
	public RestTemplate getRestTemplate(){
		RestTemplate restTemplate = new RestTemplate();
		
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));
        restTemplate.getMessageConverters().add(converter);
		return restTemplate;
	}

	public static void main(String[] args) {
		SpringApplication.run(PocdimedApplication.class, args);
		
	}

	@Override
	public void run(String... args) throws Exception {
		carregaBanco();
	}
	public void carregaBanco() throws IOException
	{
		String path = ".\\pontosDeTaxi.txt";
		File file = new File(path);
		
		if(!file.exists())
		{
			file.createNewFile();
			FileWriter fileWriter = new FileWriter(path);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			printWriter.println("PONTO-ZONA-SUL-1#-30.12373379817800000#-51.22266028234100000#2019-02-10T16:14:09.034");
			printWriter.println("PONTO-ZONA-SUL-2#-30.12273379817800000#-51.22236028234100000#2020-02-10T20:03:03.001");
			printWriter.println("PONTO-ZONA-NORTE-1#-30.12273379817800000#-51.22236028234100000#2020-01-01T11:13:28.128");
			printWriter.println("PONTO-ZONA-NORTE-2#-30.12273379817800000#-51.22236028234100000#2021-01-01T18:10:14.321");
			printWriter.println("PONTO-ZONA-LESTE-1#-30.11272379817800000#-51.12236028234100000#2021-02-01T12:09:27.424");
			printWriter.println("PONTO-ZONA-LESTE-2#-30.11273379817800000#-51.22336028234100000#2021-02-01T13:01:00.789");
			printWriter.flush();
			fileWriter.close();
		}
		
		FileReader reader = new FileReader(path);
		
		BufferedReader bufferedReader = new BufferedReader(reader);
		
		List<PontoDeTaxi> pontos = new ArrayList<>();
		
		String aux = "";
		String[] arrOfStr;
		
		while(aux != null)
		{
			aux = bufferedReader.readLine();
			if(aux != null) {
				PontoDeTaxi ponto = new PontoDeTaxi();
				arrOfStr = aux.split("#");
				ponto.setNome(arrOfStr[0]);
				ponto.setLat(Double.parseDouble(arrOfStr[1]));
				ponto.setLng(Double.parseDouble(arrOfStr[2]));
				ponto.setData(LocalDateTime.parse(arrOfStr[3]));
				pontos.add(ponto);
			}
		}
		reader.close();
		pontoDeTaxiRepository.saveAll(pontos);
	}
}
